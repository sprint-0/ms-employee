#!/bin/bash
echo "RUNNING "
set -e
eval $(ssh-agent -s)
# add private key
mkdir -p /root/.ssh
touch /root/.ssh/id_rsa
echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
chmod 700 /root/.ssh
chmod 600 /root/.ssh/id_rsa
touch ~/.ssh/config

# disable host checking
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config


git config --global user.email "$GIT_EMAIL"
git config --global user.name "$GIT_USER_NAME"
git checkout -B "$CI_COMMIT_REF_NAME"
git pull origin "$CI_COMMIT_REF_NAME"

# Get Package version
PKG_VERSION=$(cat package.json | grep version | head -1 | awk '{ print $2 }' | sed 's/[",]//g' | tr -d '[[:space:]]')

DEPLOY_SERVER=$DEPLOY_SERVER

echo ">>>> deploying to ${DEPLOY_SERVER} ${CI_ENVIRONMENT_NAME}"
# ssh and deploy
ssh centos@${DEPLOY_SERVER} 'bash -s' < ./deploy/updateAndRestart.sh "${CI_ENVIRONMENT_NAME}" "${MONGO_PASSWORD}" "${SQL_PASSWORD}" "${MS_NAME}" "${PKG_VERSION}"
