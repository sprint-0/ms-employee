import { fooRepository } from './foo.repositories';

interface FooValue {
  name: string;
}

const getFoos = async () => {
  const entities = await fooRepository.getFoos();
  return entities.map(entity => {
    const value: FooValue = {
      name: entity.name
    };
    return value;
  });
};

const fooService = {
  getFoos
};

export { fooService };
