import healthcheck from './healthcheck/healthcheck.controller';
import userController from './user/user.controller';
import employeeController from './employee/employee.controller';

const routes = [...healthcheck, ...userController, ...employeeController];

export { routes };
