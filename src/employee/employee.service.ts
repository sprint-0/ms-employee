import { ERROR_CODE } from '../common/errors';

import employeeRepository from './employee.repository';
import { IEmployee } from './employee.interface';
import { AppError } from '../errors/AppError';

const getEmployees = () => {
  return employeeRepository.get();
};

const createEmployee = async (employee: IEmployee) => {
  const existingEmployee = await employeeRepository.getFirstByName(
    employee.name
  );
  if (existingEmployee) {
    throw new AppError(ERROR_CODE.USER_NAME_EXISTED);
  }
  return employeeRepository.create(employee);
};

const updateEmployee = async (id: string, employee: IEmployee) => {
  const updatedEmployee = await employeeRepository.updateById(id, employee);
  if (updatedEmployee === null) {
    throw new AppError(ERROR_CODE.USER_NAME_NOT_EXISTED);
  }
  return updatedEmployee;
};

const deleteEmployee = async (id: string) => {
  const deletedEmployee = await employeeRepository.deleteById(id);
  if (deletedEmployee === null)
    throw new AppError(ERROR_CODE.USER_NAME_NOT_EXISTED);
  return deletedEmployee;
};

const employeeService = {
  createEmployee,
  getEmployees,
  updateEmployee,
  deleteEmployee
};
export default employeeService;
