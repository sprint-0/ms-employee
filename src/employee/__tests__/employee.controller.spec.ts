import hapi = require('@hapi/hapi');

import employeeService from '../employee.service';
import employeeController from '../employee.controller';
import { IEmployee } from '../employee.interface';
jest.mock('../employee.service');
let server: hapi.Server;

describe('employee.controller', () => {
  beforeAll(async () => {
    server = new hapi.Server();
    server.route(employeeController);
  });

  describe('GET /employees', () => {
    it('should return employeeService getEmployees with code 200', async () => {
      const testEmployees = [
        {
          _id: '5d29b6716394dea3588023d4',
          name: 'mahesh',
          gender: 'L',
          age: 20,
          __v: 0,
          id: '5d29b6716394dea3588023d4'
        }
      ];
      employeeService.getEmployees.mockResolvedValueOnce(testEmployees);
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'GET',
        url: `/employees`
      });
      expect(result.statusCode).toBe(200);
      expect(result.result).toEqual(testEmployees);
      expect(employeeService.getEmployees).toHaveBeenCalledTimes(1);
    });
  });

  describe('POST /employees', () => {
    it('should return error on empty name', async () => {
      const testEmployee: IEmployee = {
        name: '',
        age: 20
      };
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(result.statusCode).toBe(400);
      expect(result.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return error on not enough age', async () => {
      const testEmployee: IEmployee = {
        name: 'test',
        gender: 'P',
        age: 10
      };
      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(response.statusCode).toBe(400);
      expect(response.result).toEqual({
        error: 'Bad Request',
        message: 'Invalid request payload input',
        statusCode: 400
      });
    });

    it('should return 201 on valid payload and success service call', async () => {
      const testEmployee: IEmployee = {
        name: 'test',
        gender: 'L',
        age: 18
      };

      employeeService.createEmployee.mockResolvedValueOnce({
        ...testEmployee,
        id: 'new id'
      });

      const response: hapi.ServerInjectResponse = await server.inject({
        method: 'POST',
        url: `/employees`,
        payload: testEmployee
      });

      expect(response.statusCode).toBe(201);
      expect(response.result).toEqual({
        ...testEmployee,
        id: 'new id'
      });
    });
  });

  describe('PUT /employees/{id}', () => {
    it('should update employee', async () => {
      const testEmployee: IEmployee = {
        name: 'asdasd',
        gender: 'P',
        age: 20
      };
      employeeService.updateEmployee.mockReturnValueOnce({
        ...testEmployee,
        id: '123456'
      });
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'PUT',
        url: `/employees/123456`,
        payload: testEmployee
      });
      expect(employeeService.updateEmployee).toBeCalledTimes(1);
      expect(result.result).toEqual({
        ...testEmployee,
        id: '123456'
      });
      expect(result.statusCode).toBe(200);
    });
  });

  describe('DELETE /employees/{id}', () => {
    it('should delete employee', async () => {
      const testEmployee: IEmployee = {
        name: 'asdasd',
        gender: 'P',
        age: 20
      };
      employeeService.deleteEmployee.mockReturnValueOnce({
        ...testEmployee,
        id: '123456'
      });
      const result: hapi.ServerInjectResponse = await server.inject({
        method: 'DELETE',
        url: `/employees/123456`,
        payload: testEmployee
      });
      expect(employeeService.deleteEmployee).toBeCalledTimes(1);
      expect(result.result).toEqual({
        ...testEmployee,
        id: '123456'
      });
      expect(result.statusCode).toBe(200);
    });
  });
});
