import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';

import { EmployeeModel } from '../employee.model';
import employeeRepository from '../employee.repository';

jest.mock('mongoose', () => {
  const mongoose = require.requireActual('mongoose');
  return new mongoose.Mongoose(); // new mongoose instance and connection for each test
});

describe('employee.repository', () => {
  let mongod: MongoMemoryServer;
  beforeAll(async () => {
    mongod = new MongoMemoryServer();
    const mongoDbUri = await mongod.getConnectionString();
    await mongoose.connect(mongoDbUri, { useNewUrlParser: true });
  });

  afterAll(async () => {
    mongoose.disconnect();
    mongod.stop();
  });

  describe('get', () => {
    it('should get all employees', async () => {
      await EmployeeModel.create(
        {
          name: 'u1',
          gender: 'L',
          age: 20
        },
        {
          name: 'u2',
          gender: 'L',
          age: 20
        }
      );

      const employees = await employeeRepository.get();
      expect(employees).toHaveLength(2);
    });
  });

  describe('create', () => {
    it('should create new employee', async () => {
      const newEmployee = await employeeRepository.create({
        name: 'new employee',
        gender: 'P',
        age: 18
      });
      expect(newEmployee.id).toBeDefined();
    });
  });

  describe('update', () => {
    it('should update employee', async () => {
      await EmployeeModel.create({
        name: 'u1',
        gender: 'L',
        age: 20
      });

      const oldEmployee = await EmployeeModel.findOne({ name: 'u1' });

      const updatedEmployee = await employeeRepository.updateById(
        oldEmployee.id,
        {
          name: 'u2',
          gender: 'P',
          age: 21
        }
      );

      expect(updatedEmployee.name).toEqual('u2');
      expect(updatedEmployee.gender).toEqual('P');
      expect(updatedEmployee.age).toEqual(21);
    });
  });

  describe('update', () => {
    it('should update employee', async () => {
      await EmployeeModel.create({
        name: 'u1',
        gender: 'L',
        age: 20
      });

      const oldEmployee = await EmployeeModel.findOne({ name: 'u1' });

      const deletedEmployee = await employeeRepository.deleteById(
        oldEmployee.id
      );

      expect(await EmployeeModel.findById(oldEmployee.id)).toBeNull();

      expect(deletedEmployee.name).toEqual('u1');
      expect(deletedEmployee.gender).toEqual('L');
      expect(deletedEmployee.age).toEqual(20);
    });
  });

  describe('getFirstByName', () => {
    it('should return first employee if name exists in db', async () => {
      await EmployeeModel.create({
        name: 'exist name',
        gender: 'P',
        age: '18'
      });
      const employee = await employeeRepository.getFirstByName('exist name');
      expect(employee.name).toEqual('exist name');
    });

    it('should return null if name not exists in db', async () => {
      const employee = await employeeRepository.getFirstByName(
        'not exist name'
      );
      expect(employee).toBeNull();
    });
  });
});
