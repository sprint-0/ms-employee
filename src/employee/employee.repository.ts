import { EmployeeModel, EmployeeDocument } from './employee.model';
import { IEmployee, IEmployeeResponse } from './employee.interface';

const employeeDocumentToObject = (document: EmployeeDocument) =>
  document.toObject({ getters: true }) as IEmployeeResponse;

const employeeDocumentsToObjects = (documents: EmployeeDocument[]) =>
  documents.map(employeeDocumentToObject);

const get = async () => {
  const documents = await EmployeeModel.find().exec();
  return employeeDocumentsToObjects(documents);
};

const create = async (employee: IEmployee) => {
  const newEmployee = new EmployeeModel(employee);

  await newEmployee.save();

  return employeeDocumentToObject(newEmployee);
};

const updateById = async (id: string, employee: IEmployee) => {
  const updatedEmployee = await EmployeeModel.findByIdAndUpdate(id, employee, {
    new: true
  });
  return updatedEmployee && employeeDocumentToObject(updatedEmployee);
};

const deleteById = async (id: string) => {
  const deletedEmployee = await EmployeeModel.findByIdAndDelete(id);
  return deletedEmployee && employeeDocumentToObject(deletedEmployee);
};

const getFirstByName = async (name: string) => {
  const employee = await EmployeeModel.findOne({ name }).exec();
  return employee && employeeDocumentToObject(employee);
};

const employeeRepository = {
  get,
  create,
  updateById,
  deleteById,
  getFirstByName
};
export default employeeRepository;
