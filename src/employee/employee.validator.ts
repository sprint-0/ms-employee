import * as Joi from '@hapi/joi';

import employeeConstant from './employee.constant';

import { MongooseBase } from '../common/validators';

const EmployeeValidator = {
  name: Joi.string().required(),
  gender: Joi.string()
    .trim()
    .regex(RegExp('L|P'))
    .length(1)
    .required(),
  age: Joi.number()
    .min(employeeConstant.MIN_EMPLOYEE_AGE)
    .required(),
  wage: Joi.number().min(employeeConstant.MIN_EMPLOYEE_WAGE)
};

const EmployeeResponseValidator = Joi.object({
  ...MongooseBase,
  ...EmployeeValidator
})
  .required()
  .label('Response - Employee');

const EmployeeListResponseValidator = Joi.array()
  .items(EmployeeResponseValidator)
  .label('Response - Employees');

const createEmployeeRequestValidator = Joi.object({
  ...EmployeeValidator
}).label('Request - new employee');

const employeeIdRequestValidator = Joi.object({
  id: Joi.string().required()
}).label('Request - employee id');

export {
  EmployeeResponseValidator,
  createEmployeeRequestValidator,
  employeeIdRequestValidator,
  EmployeeListResponseValidator,
  EmployeeValidator
};
