import hapi from '@hapi/hapi';
import { Http } from '@dk/module-common';
import employeeService from './employee.service';
import {
  EmployeeListResponseValidator,
  createEmployeeRequestValidator,
  EmployeeResponseValidator,
  employeeIdRequestValidator
} from './employee.validator';
import { IEmployeeRequest } from './employee.interface';

const getEmployee: hapi.ServerRoute = {
  method: Http.Method.GET,
  path: '/employees',
  options: {
    description: 'Get the list of employees',
    notes: 'Get the list of employees',
    tags: ['api', 'employees'],
    response: {
      schema: EmployeeListResponseValidator
    },
    handler: () => {
      return employeeService.getEmployees();
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.OK]: {
            description: 'Employees retrieved'
          }
        }
      }
    }
  }
};

const createEmployee: hapi.ServerRoute = {
  method: Http.Method.POST,
  path: '/employees',
  options: {
    description: 'Create new employee',
    notes: 'All information must valid',
    validate: {
      payload: createEmployeeRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const newEmployee = await employeeService.createEmployee(
        hapiRequest.payload
      );
      return hapiResponse.response(newEmployee).code(Http.StatusCode.CREATED);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee created.'
          }
        }
      }
    }
  }
};

const updateEmployee: hapi.ServerRoute = {
  method: Http.Method.PUT,
  path: '/employees/{id}',
  options: {
    description: 'Update employee',
    notes: 'All information must valid',
    validate: {
      payload: createEmployeeRequestValidator,
      params: employeeIdRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const updatedEmployee = await employeeService.updateEmployee(
        hapiRequest.params.id,
        hapiRequest.payload
      );
      return hapiResponse.response(updatedEmployee).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee updated.'
          }
        }
      }
    }
  }
};

const deleteEmployee: hapi.ServerRoute = {
  method: Http.Method.DELETE,
  path: '/employees/{id}',
  options: {
    description: 'Update employee',
    notes: 'All information must valid',
    validate: {
      params: employeeIdRequestValidator
    },
    response: {
      schema: EmployeeResponseValidator
    },
    tags: ['api', 'employees'],
    handler: async (
      hapiRequest: IEmployeeRequest,
      hapiResponse: hapi.ResponseToolkit
    ) => {
      const deletedEmployee = await employeeService.deleteEmployee(
        hapiRequest.params.id
      );
      return hapiResponse.response(deletedEmployee).code(Http.StatusCode.OK);
    },
    plugins: {
      'hapi-swagger': {
        responses: {
          [Http.StatusCode.CREATED]: {
            description: 'Employee deleted.'
          }
        }
      }
    }
  }
};

const employeeController: hapi.ServerRoute[] = [
  getEmployee,
  createEmployee,
  updateEmployee,
  deleteEmployee
];
export default employeeController;
