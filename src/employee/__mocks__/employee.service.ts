export default {
  createEmployee: jest.fn(),
  getEmployees: jest.fn(),
  updateEmployee: jest.fn(),
  deleteEmployee: jest.fn()
};
