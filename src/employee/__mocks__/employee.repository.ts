export default {
  get: jest.fn(),
  create: jest.fn(),
  updateById: jest.fn(),
  deleteById: jest.fn(),
  getFirstByName: jest.fn()
};
